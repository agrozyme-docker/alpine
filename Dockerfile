FROM docker.io/library/alpine

RUN apk add --no-cache bash ruby-rdoc ruby-bundler
SHELL [ "/bin/bash", "-c" ]
COPY rootfs /

RUN chmod +x /usr/local/bin/* \
#  && gem install -N thor \
  && gem install -N docker_core \
  && /usr/local/bin/docker_build.rb

ENTRYPOINT ["/sbin/tini-static", "--"]
CMD ["/bin/bash"]
