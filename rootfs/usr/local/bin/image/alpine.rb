# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Alpine
      module Build
        def self.add_packages
          System.run('apk add --no-cache tini-static su-exec patch git')
        end

        def self.update_repositories
          file = '/etc/apk/repositories'
          text = File.read(file)
          text = text.gsub(/^#(http.*)$/, '\1')
          text = text.gsub(/^(http.*\/edge\/.*)$/, '#\1')
          File.write(file, text)
        end

        def self.update_profile
          profile = '/etc/profile.d'

          Shell.find_paths("#{profile}/*.sh.disabled").each do |file|
            name = File.basename(file, '.*')
            File.rename(file, "#{profile}/#{name}")
          end

          File.symlink('/etc/profile', "#{ENV['HOME']}/.bashrc")
        end

        def self.update_crontab
          one_minute = '/etc/periodic/1min'

          Shell.make_folders(one_minute)
          File.open('/etc/crontabs/root', 'a') do |file|
            file.puts("*       *       *       *       *       run-parts #{one_minute}")
          end
        end

        def self.main
          Shell.update_user
          System.invoke('Update Repositories', self.method(:update_repositories))
          System.invoke('Add Packages', self.method(:add_packages))
          System.invoke('Update Profile', self.method(:update_profile))
          System.invoke('Update Crontabs', self.method(:update_crontab))
          Color.echo(Etc.uname, Color::CYAN)
        end

      end
    end
  end
end
