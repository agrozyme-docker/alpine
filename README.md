# Summary

Source: https://gitlab.com/agrozyme-docker/alpine

Alpine Base Image

# Packages

- ruby
- tini-static
- su-exec
- bash
- patch
- git

# Environment Variables

- DOCKER_CORE_UID
- DOCKER_CORE_GID

# Core User

- Run the container with the `root` user
- Run the service in the container with the `core` user
- Simply map the host OS user by setting environment variables `DOCKER_CORE_UID` / `DOCKER_CORE_GID`
    - default UID: `500`
    - default GID: `500`
- Use the ruby gem `docker_core` to call `update_user` to change UID / GID of `core`
  using the environment variable `DOCKER_CORE_UID` / `DOCKER_CORE_GID`
- If the service can not be run as a custom user, it can use `su-exec core` to execute the service

# Docker Build

`Dockerfile` example

```dockerfile
FROM docker.io/library/alpine

RUN apk add --no-cache bash ruby-rdoc ruby-bundler
SHELL [ "/bin/bash", "-c" ]
COPY rootfs /

RUN chmod +x /usr/local/bin/* \
#  && gem install -N thor \
  && gem install -N docker_core \
  && /usr/local/bin/docker_build.rb

ENTRYPOINT ["/sbin/tini-static", "--"]
CMD ["/bin/bash"]
```

## /usr/local/bin/image

- Put custom ruby module scripts here
- Add the statement `require_relative("./image/#{module}")` to other scripts (ex: `docker_build.rb` / `docker_run.rb`)

## /usr/local/bin/docker_build.rb

- The `docker build` script
- Add the
  statement `RUN chmod +x /usr/local/bin/* && gem update -N docker_core && gem clean && /usr/local/bin/docker_build.rb`
  to `Dockerfile`

Example

```rb
#! /usr/bin/ruby

def main
  print(`apk add --no-cache su-exec tini-static busybox-suid`)
end

main
```

# Docker Run

## /usr/local/bin/docker_run.rb

- The `docker run` script
- Add the statement `CMD ["/usr/local/bin/docker_run.rb"]` to `Dockerfile`

Example

```rb
#! /usr/bin/ruby

def main
  exec('/bin/bash')
end

main
```

## /usr/local/bin

- Put the command binary / script here
- Use `docker run -it --rm {image} {command}` to execute the command

# Host scripts

- Run at host OS or VM
- See https://gitlab.com/agrozyme-package/ruby/docker_command example/stack folder

## Docker Files Layout

```
/home/core/deploy
|-- profile.sh
|-- runner.rb
|-- <core>
|-- <volume>
|-- <stack>
    |-- docker-compose.yml
    |-- [setup.sh]
    |-- [*.env]
```

## Kubernetes Files Layout

```
/home/core/deploy
|-- profile.sh
|-- runner.rb
|-- <core>
|-- <volume>
|-- <stack>
    |-- kustomization.yml
    |-- volume.yml
    |-- issuer.yml
    |-- deployment.yml
    |-- [setup.sh]
    |-- [*.env]
```

## Prepare

- If the `core` user does not exist in the host OS, create one
- It is recommended to set the uid / gid of the `core` user to `500`
- The example files in the `host` folder need to be placed in the `/home/core/deploy` directory
- install ruby (version 2.5+)
- install `docker_core` gem
- clone https://gitlab.com/agrozyme-package/ruby/docker_command and change for your needs
- copy `deploy` folder to `/home/core/deploy` in host OS or VM

### Commands

- `update_core`: update gem docker_core
- `update_alias`: update alias
- `update_swarm [orchestrator]`: update default orchestrator
- `swarm_run`: run command
- `swarm_status`: show swarm service status
- `swarm_setup`: setup the swarm
- `swarm_leave`: clean everything up and leave the swarm
- `stack_deploy`: delpoy stack
- `stack_remove`: remove stack and containers

# Gitlab CI

- Build a multi-platform docker image and push it to [Docker Hub](https://hub.docker.com/)
- Use [podman-qemu](https://gitlab.com/agrozyme-docker/podman-qemu) to build
